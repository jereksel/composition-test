package pl.jereksel.composition.models

class PhotoWithComments(
    val id: String,
    val name: String,
    val url: String,
    val comments: List<Comment>
)