package pl.jereksel.composition.models

import java.util.*

data class Album(
    val id: String,
    val name: String,
    val userId: String,
    val creationDate: Date,
    val linkedAlbums: List<String>,
    val photos: List<Photo>
)